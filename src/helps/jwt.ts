import * as jwt from 'jsonwebtoken';

const secret : string = process.env.KEY_JWT ? process.env.KEY_JWT : 'secret-key';

export function sign(payload: any) {
  const token = jwt.sign(payload, secret);
  return token;
}

export function verify(token: string) {
  try {
    return jwt.verify(token, secret);
  } catch (err) {
    return false;
  }
}

export function decode(token: string) {
  return jwt.decode(token, {complete: true});
}