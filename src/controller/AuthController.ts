import { google } from 'googleapis';
import * as jwt from '../helps/jwt';
import { md5 } from '../helps/crypto';
import { setKey, deleteKey } from '../helps/redis';
import { typeDeviceConnect } from '../const/typeDeviceConnect';
import { getBrowser } from '../helps/getBrowser';

interface Payload {
  username: string;
  email: string;
  browser?: string;
}
export class AuthController<T>
{
  private readonly MAIL_ALLOW = ['vnext.com.vn', 'vnext.vn'];
  private oauth2Client = new google.auth.OAuth2(
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET,
    process.env.REDIRECT_URI
  );

  private readonly UserRepo: any;
  constructor(UserRepo: any) {
    this.UserRepo = UserRepo;
  }

  public getGoogleLoginPageURI(req : any, res: any, next: any) {
    const url = this.oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: 'https://www.googleapis.com/auth/plus.me',
      include_granted_scopes: true,
      response_type: 'code',
      prompt: 'consent'
    });
    res.redirect(url, next);
  }

  public async getDataFromLoginRedirect(req : any, res: any, next: any) {
    const { code }= req.query;
    const {tokens} = await this.oauth2Client.getToken(code);
    const idToken = tokens.id_token ? tokens.id_token: '';
    const getInfo =  idToken.split('.');
    const profile : any = JSON.parse(this.oauth2Client.decodeBase64(getInfo[1]));
    // check mail vnext
    const mailCompany = profile.hd;
    if (this.MAIL_ALLOW.indexOf(mailCompany) === -1) {
      res.send(401, {message: 'Only allow mail vnext.'});
      return next();
    }
    // check user in db
    const user = await this.UserRepo.findBy('email', profile.email);
    if (!user) {
      res.send(401, {message: 'Not found user'});
      return next();
    }
    const browser = getBrowser(req);
    const payload = {
      email: profile.email,
      username: user.username,
      browser
    }
    const jwtToken = await this.createJWT(payload);
    res.send({ token: jwtToken });
  }

  public async login(req : any, res: any, next: any) {
    const { username, password } = req.body;
    if (!username || !password) {
      res.send(400, {message: 'Username and password is required.'});
      return next();
    }

    const user = await this.UserRepo.auth({username, password});
    if (!user) {
      res.send(400, {message: 'Invalid username and password.'});
      return next();
    }
    let browser = getBrowser(req);
    browser = browser ? browser : 'unknown';
    const payload = {
      email: user.email,
      username: user.username,
      browser
    }
    const jwtToken = await this.createJWT(payload);
    res.send({ token: jwtToken });
  }

  private async createJWT(payload: Payload) {
    // create data token
    const nonce = Math.floor((Math.random() * 100) + 1);
    const suffix = `${payload.browser}-${nonce}`;
    const dataToken = { ...payload, suffix };
    delete dataToken.browser;
    const hashDataToken = md5(JSON.stringify(dataToken));
    // save on redis
    setKey(dataToken.username, hashDataToken, suffix);
    // create token
    const jwtToken = jwt.sign({data: dataToken, hash: hashDataToken});
    return jwtToken;
  }

  public async logout(req : any, res: any, next: any) {
    const userCurrent = req.get('user');
    let browser = getBrowser(req);
    let suffix;
    if (typeDeviceConnect[browser]) {
      suffix = `${typeDeviceConnect[browser]}`;
    } else {
      suffix = 'all';
    }
    try {
      const result = await deleteKey(userCurrent.username, suffix);
      if (result) return res.send('logout success');
    } catch (error) {
      console.log(error);
    }
    res.send(400,'errors');
  }
}